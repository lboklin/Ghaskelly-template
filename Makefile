LIBFILE = $(shell stack path --local-install-root)/lib/libghaskelly-game.so
PROJECTROOT = $(shell stack path --project-root)
all:
	stack clean ghaskelly-game
	stack build
	cp $(LIBFILE) $(PROJECTROOT)/game/lib
run:
	stack build
	cp $(LIBFILE) $(PROJECTROOT)/game/lib
	godot --path ./game

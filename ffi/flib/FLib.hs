{-# LANGUAGE ForeignFunctionInterface #-}
module FLib where

import           Data.Coerce     (coerce)

import           Foreign         (peek)

import           Ghaskelly
import           Ghaskelly.Godot

import           Game            (game)



godot_nativescript_init :: GdnativeHandle -> IO ()
godot_nativescript_init desc = do
  putLText "Haskell NativeScript initialized"
  initGhaskelly desc game

foreign export ccall godot_nativescript_init :: GdnativeHandle -> IO ()



godot_gdnative_init :: GodotGdnativeInitOptionsPtr -> IO ()
godot_gdnative_init optPtr = do
  putLText "Haskell GDNative initialized"
  opt <- peek optPtr
  initApiStructs opt

foreign export ccall godot_gdnative_init :: GodotGdnativeInitOptionsPtr -> IO ()



godot_gdnative_terminate :: GodotGdnativeTerminateOptionsPtr -> IO ()
godot_gdnative_terminate optPtr = putLText "Haskell GDNative terminated"

foreign export ccall godot_gdnative_terminate :: GodotGdnativeTerminateOptionsPtr -> IO ()

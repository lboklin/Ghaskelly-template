{-# LANGUAGE TupleSections #-}
{-# LANGUAGE LambdaCase     #-}
module Game where

import Ghaskelly
import Ghaskelly.Godot hiding (update, show, print)

import Ghaskelly.Cmd as Cmd
import Ghaskelly.Sub as Sub


game :: Game Model Msg
game = Game
  { current = (initModel, initCmd)
  , update = update'
  , subscriptions = subscriptions'
  }


-- MODEL


data Model = Model { time :: Float }


initModel :: Model
initModel = Model 0.0

initCmd :: [IO Msg]
initCmd =
  [ EmptyMsg <$ print "Ghaskelly is _ready()" ]


-- UPDATE


data Msg
  = EmptyMsg
  | Input GodotNode GodotInputEvent
  | Process Float
  | InputAction Action

data Action
  = NoAction
  | QuitGame GodotNode


update' :: Msg -> Model -> (Model, Cmd Msg)
update' = \case
  EmptyMsg ->
    Cmd.none

  Input self event ->
    Cmd.cmd $ InputAction <$> fromInput self event

  Process delta ->
    \(Model t) -> Cmd.none $ Model (t + delta)

  InputAction action ->
    case action of
      NoAction ->
        Cmd.none

      QuitGame nd ->
        Cmd.cmd $ EmptyMsg <$ do
          nd & get_tree >>= quit
          print "Quitting game.."


fromInput :: GodotNode -> GodotInputEvent -> IO Action
fromInput self event@(GodotInputEvent ievObj) = do
  isInputEventKey <- is_class event #<< "InputEventKey"
  isPressed <- is_pressed event

  if isInputEventKey && isPressed
    then do
      scancode <- get_scancode (GodotInputEventKey ievObj)
      return $
        case scancode of
          KEY_ESCAPE  ->
            QuitGame self

          _ ->
            NoAction
    else
      return NoAction


-- SUBSCRIPTIONS


subscriptions' :: Model -> Sub Msg
subscriptions' _ =
  Sub.batch
    [ Sub.onInput $ \self -> Input $ GodotNode self
    , Sub.onProcess $ \_ -> Process
    ]
